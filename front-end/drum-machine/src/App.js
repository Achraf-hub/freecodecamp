import React from 'react';
import "./App.css";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            playingSoundName: "The playing sound 0",
        };
        this.handlePlay = this.handlePlay.bind(this);
    }
    handlePlay(name){
        this.setState({
            playingSoundName: name
        });
    }
    render() {
        const names = ["Q", "W", "E", "A", "S", "D", "Z", "X", "C"];
        return <div>
            <main id="drum-machine">
                <Display playingSoundName={this.state.playingSoundName} />
                <div className="DrumPadContainer">
                {names.map((name) => <DrumPad key={name} name={name} onPlay={this.handlePlay} />)}
                  </div>
            </main>
        </div>;
    }
}

const Display = props => <div id="display">{props.playingSoundName}</div>;

const DrumPad = props => {
    
       const BASE_URL = "../assets/collision.wav"; 
        
    const playSound = (e) => {
        props.onPlay("playing-"+props.name);
        const audioElement = document.getElementById(props.name);
        audioElement.currentTime = 0;
        audioElement.play();
    };
    const handleClick = (e) =>{
        playSound(e);
    };
    const handleKeyDown = (e) => {
        if (e.key == props.name) {
            playSound(e);
        }
    };
    
    return <div className="drumPad" id={"sound-" + props.name} onClick={handleClick} onKeyDown={handleKeyDown} >
        {props.name}
        <audio id={props.name} className="clip" src={BASE_URL}  ></audio>
    </div>;
};


export default App;