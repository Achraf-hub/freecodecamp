import React from 'react';
import marked from "marked";
//import Redux from 'redux';
//import redux-react from "redux-react";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            markdownText: `# Welcome to my Markdown Previewer  
## How to use ? 
just type Markdown in editor and it will render 
##github
you can visit **my** [github account](https://github.com/Achraf-hub) yes my name is Achraf
To log some thing \`console.log()\` 
to create a  function 
\`\`\` 
function(arg){}
\`\`\`
## some advice 
> don't say anything when you are angry  
## some todo 
1. write code 
1. debug code
1. deploy code 
![an image](https://cdn.freecodecamp.org/curriculum/cat-photo-app/relaxing-cat.jpg) `
        }

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            markdownText: event.target.value
        });
    }

    render() {
        return <div style={{}}>
    <h1> Markdown Previewer </h1>
    <div className="editor-preview">
      <h2>Editor</h2>
      <textarea value={this.state.markdownText} onChange={this.handleChange} id="editor" style={{border: "2px solid #4200ff", width: "80%", height: "200px", alignContent: "center"}} ></textarea>
        </div>
    <div>
      <h2>Preview</h2>
      <div id="preview" dangerouslySetInnerHTML={{__html: marked.parse(this.state.markdownText)}} ></div>
        </div>
        </div>;
    }
}

export default App;