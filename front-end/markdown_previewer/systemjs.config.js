SystemJS.config({
  baseURL:'https://unpkg.com/',
  defaultExtension: true,
  packages:{
    "/": "js"
  },
  meta: {
    '*.js': {
      'babelOptions': {
        react: true
      }
    },
    '*.css': {
      loader: 'css'
    },
  },
  map: {
    'plugin-babel': 'systemjs-plugin-babel@latest/plugin-babel.js',
    'systemjs-babel-build': 'systemjs-plugin-babel@latest/systemjs-babel-browser.js',
    'react': 'react@16.4.2/umd/react.development.js',
    'react-dom': 'react-dom@16.4.2/umd/react-dom.development.js',
    'css': 'systemjs-plugin-css@latest/css.js',
    "marked": "marked@latest/marked.min.js"
    
  },
  transpiler: 'plugin-babel'
});

//SystemJS.import('https://cdnjs.com/libraries/marked')
SystemJS.import('./src/index.js')
  .catch(console.error.bind(console));