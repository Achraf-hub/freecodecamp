/* abandonned
//todo: add a method to load quotes done
let quotes = null;
// load quotes 
function loadQuotes(path, wraper) {
  $.getJson(path, function(data){
    quotes = data.quotes;
  });
}
*/
//todo: add a method to change the quote done
function updateQuote(quotes) {
  const quote = quotes[Math.round(Math.random() * quotes.length)];
  $("#text").html(quote.quote);
  $("#author").html(quote.author);
}
//todo: add new quote on load and bind events
$(document).ready(function(){
  let quotes = null;
    $.getJSON("data/quotes.json", function(data){
    quotes = data.quotes;
    updateQuote(quotes);
  });
  $("#new-quote").on("click", function(e){
    updateQuote(quotes);
  });
  
});
