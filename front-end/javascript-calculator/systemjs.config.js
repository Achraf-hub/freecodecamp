SystemJS.config({
  baseURL:'node_modules/',
  defaultExtension: true,
  packages:{
    "/": "js"
  },
  meta: {
    '*.js': {
      'babelOptions': {
        react: true
      }
    },
    '*.css': {
      loader: 'css'
    },
  },
  map: {
    'plugin-babel': 'systemjs-plugin-babel/plugin-babel.js',
    'systemjs-babel-build': 'systemjs-plugin-babel/systemjs-babel-browser.js',
    'react': 'react/umd/react.development.js',
    'react-dom': 'react-dom/umd/react-dom.development.js',
    'css': 'systemjs-plugin-css/css.js'
  },
  transpiler: 'plugin-babel'
});

SystemJS.import('./src/index.js')
  .catch(console.error.bind(console));