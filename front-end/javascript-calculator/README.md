# Welcome to my freecodecamp JavaScript calculator project

This README.md file acts as a workbook for the project

Note

1. I need a container for my book that will be a main element nested in a div.container element
2.  I will create a div#display element where the output will be displayed
3.  I will create a div.buttonContainer element which contains all my calculator buttons
4.  I will create a "calculatorButton" Functional Component which will be our button base conponent
5.  I will create a Component for OperatorButtons
6.  I will create a conponent for NumerButtons
7.  Other Buttons will have their own behavior