function palindrome(word) {
    //preparing the word
    word = word.replace(/_/g, "");
    word = word.match(/\w/g).join('');
    word = word.toLowerCase();

    // checking
    let wordInverted = "";
    for (let letter of word) {
        wordInverted = letter+wordInverted;
    }

    return (word == wordInverted);
}