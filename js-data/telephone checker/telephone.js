function telephoneCheck(str) {

    let valide = [
        /1 \d\d\d-\d\d\d-\d\d\d\d/,
        /1 \(\d\d\d\) \d\d\d-\d\d\d\d/,
        /\d\d\d\d\d\d\d\d\d\d/,
        /\d\d\d-\d\d\d-\d\d\d\d/,
        /\(\d\d\d\)\d\d\d-\d\d\d\d/,
        /1\(\d\d\d\)\d\d\d-\d\d\d\d/,
        /1 \d\d\d \d\d\d \d\d\d\d/,
        /\(\d\d\d\) \d\d\d-\d\d\d\d/,
        /\d\d\d \d\d\d \d\d\d\d/,
        /\d\d\d\d\d\d\d\d\d\d/,
        /1 \d\d\d \d\d\d \d\d\d\d/,
        /1 \(\d\d\d\) \d\d\d-\d\d\d\d/,
        /\(\d\d\d\)\d\d\d-\d\d\d\d/
    ];

    for (let reg of valide) {
    
       // console.log(reg.toString().replace(/\\/g, ""), reg, str, reg.test(str));

        if (reg.test(str) && reg.toString().replace(/\\/g, "").length == (str.length+2) ){
            return true;
        }
    }
    
    return false;
}
    console.log(telephoneCheck("5555555555"));