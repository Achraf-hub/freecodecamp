function checkCashRegister(price, cash, cid) {
    
    // currency values
    let crrcval = [
        ["PENNY",
            0.01],
        ["NICKEL",
            0.05],
        ["DIME",
            0.1],
        ["QUARTER",
            0.25],
        ["ONE",
            1],
        ["FIVE",
            5],
        ["TEN",
            10],
        ["TWENTY",
            20],
        ["ONE HUNDRED",
            100]
    ];

    let status = "CLOSED";
    let change = [];
    let due = (cash - price)*100;
    
    cid = cid.map((item)=>[item[0], item[1]*100]);
    crrcval = crrcval.map((item)=>[item[0], item[1]*100]);
    
    console.log(cid, crrcval);
    for (let i = cid.length-1; i >= 0; i--) {
        if (due >= cid[i][1]) {
            due -= cid[i][1];
            change.unshift([cid[i][0], cid[i][1]]);
            cid[i][1] = 0;
            //console.log(1, due, cid[i][1]);
        } else if (cid[i][1] > 0) {
            status = "OPEN";
            let needed = due - due%crrcval[i][1];
            due -= needed;
            cid[i][1] -= needed;
            change.unshift([cid[i][0], needed]);
        }
    }

    if (due > 0) {
        return {
            status: "INSUFFICIENT_FUNDS",
            change: []};
    }

    change = change.map((item)=>[item[0], item[1]/100]);

    if (status == "OPEN") {
        change = change.filter((item)=>item[1]!=0).reverse();
    }

    return {
        status,
        change
    };
}


console.log(checkCashRegister(19.5, 20, [["PENNY", 0.5], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]]));